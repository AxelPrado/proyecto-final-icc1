package mx.icc.utilidades.controladores;

import java.io.*;
import java.util.*;
/**
 * Esta clase permite manejar de manera adecuada
 * las cadenas de texto de un archivo.
 * El manejo es por lineas almacenadas en una lista.
 * @author Axel Prado
 * @version 1.0, 28/11/2019
 */
public class ManejadorDeArchivos {
    /**
     * Archivo que se va a gestionar.
     */
    protected File archivo;
    
    /**
     * Constructor de un argumento.
     * Verifica si el archivo existe, si no, crea uno vacío.
     * Asigna el valor del parámetro al atributo archivo.
     * @param archivo File que será manipulado
     * @throws IOException
     */
    public ManejadorDeArchivos(File archivo) throws IOException {
        if (archivo.exists()) {
            this.archivo = archivo;
        } else {
            archivo.createNewFile();
            this.archivo = archivo;
        }
    }
    
    /**
     * Método que obtiene una lista de las lineas del archivo de texto.
     * Si el archivo está vacío, crea una lista vacía.
     * @return el arreglo de las lineas del archivo.
     * @throws IOException 
     */
    public ArrayList<String> obtenerListaLineas() throws IOException {
        ArrayList<String> listaLineas = new ArrayList<String>();
        FileReader archivoReader = new FileReader(this.archivo);
        BufferedReader archivoBfR = new BufferedReader(archivoReader);
        String linea;
        while ( (linea = archivoBfR.readLine()) != null) {
            listaLineas.add(linea);
        }   
        archivoBfR.close();
        return listaLineas;
    }
    
    /**
     * Método que obtiene el número de lineas de un archivo de texto.
     * @return el número de lineas de un archivo.
     * @throws IOException 
     */
    public int númeroDeLineas() throws IOException {
        ArrayList<String> listaLineas = this.obtenerListaLineas();
        return listaLineas.size();
                
    }
    
    /**
     * Método que inserta una linea de texto en el archivo.
     * La linea donde será insertado el texto es indicada por un índice.
     * El indice se reduce en 1 debido a que la lista empieza a indexar desde 0.
     * Si la lista es vacía (archivo vacío), se inserta una linea en el archivo.
     * Si el índice es mayor que el número de lineas, se incerta una linea al final del archivo. 
     * @param nuevaLinea cadena que será insertada en el archivo.
     * @param númeroLinea posición donde será insertada la nueva linea.
     * @throws IOException 
     */
    public void insertarLinea(String nuevaLinea,int númeroLinea) throws IOException {
        ArrayList<String> listaActual = this.obtenerListaLineas();
        FileWriter archivoWriter = new FileWriter(this.archivo,false);
        BufferedWriter archivoBfW = new BufferedWriter(archivoWriter);
        if (listaActual.isEmpty() || (númeroLinea  >= listaActual.size())) {
            listaActual.add(nuevaLinea);
        } else {
            listaActual.add(númeroLinea - 1,nuevaLinea);
        } 
        
        for (String str : listaActual) {
            archivoBfW.write(str);
            archivoBfW.newLine();
        }
        archivoBfW.close();
    }
    
    /**
     * Método que elimina una linea de texto en el archivo.
     * La linea donde que será eliminada del texto es indicada por un índice.
     * El indice se reduce en 1 debido a que la lista empieza a indexar desde 0.
     * Si el índice es mayor que el número de lineas no se elimina ninguna linea.
     * Si el archivo es vacío, no se elimina ninguna linea.
     * @param númeroLinea posición de la linea que será eliminada.
     * @return linea eliminada o una cadena vacía.
     * @throws IOException 
     */
    public String eliminarLinea(int númeroLinea) throws IOException {
        String lineaEliminada = "";
        ArrayList<String> listaActual = this.obtenerListaLineas();
        FileWriter archivoWriter = new FileWriter(this.archivo,false);
        BufferedWriter archivoBfW = new BufferedWriter(archivoWriter);
        if ( !listaActual.isEmpty() &&  !(númeroLinea  >= listaActual.size())) {
            lineaEliminada = listaActual.remove(númeroLinea - 1);      
        } 
        for (String str : listaActual) {
            archivoBfW.write(str);
            archivoBfW.newLine();
        }
        archivoBfW.close();
        return lineaEliminada;
    }
    
    
}
