package mx.icc.entidades;

/**
 * Una clase que representa un usuario de la aplicación.
 * Cada Usuario queda representado por:
 * nombre, apellido, un Id, una contraseña y los permisos de acceso.
 * @author Axel Prado
 * @version 1.0, 27/11/2019
 */
public class Usuario {

    /**
     * Nombre del usuario.
     */
    protected final String nombre;
    /**
     * Apellido del usuario.
     */
    protected final String apellido;
    /**
     * Identificador del usuario para usar la aplicación.
     */
    protected String idUsuario;
    /**
     * Contraseña que protege la cuenta del usuario.
     */
    protected String contraseña;
    /**
     * Hay dos tipos de permisos:
     * 1) user: usuario estandar
     * 2) admin: administrador
     */
    protected String permisoUsuario;
    
    /**
     * Constructor de un usuario con 5 parámetros.
     * Inicializa todos los atributos con los parámetros dados.
     * @param nombre Argumento que representa el nombre del ususario.
     * @param apellido Argumento que representa el apellido del usuario.
     * @param idUsuario Argumento que representa el Id de ussuario.
     * @param contraseña Argumento que representa la contraseña del usuario.
     * @param permisoUsuario Argumento que representa el permiso como usuario.
     */
    public Usuario(String nombre, String apellido, String idUsuario, String contraseña, String permisoUsuario) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.idUsuario = idUsuario;
        this.contraseña = contraseña;
        this.permisoUsuario = permisoUsuario;
    }
    
    /**
     * Método para obtener el nombre del usuario.
     * @return Nombre del usuario.
     */
    public String getNombre() {
        return this.nombre;
    }
    
    /**
     * Método para obtener el apellido del usuario.
     * @return Apellido del usuario.
     */
    public String getApellido() {
        return this.apellido;
    }
    
    /**
     * Método para obtener el Id de usuario.
     * @return Id de usuario.
     */
    public String getIdUsuario() {
        return this.idUsuario;
    }
    
    /**
     * Método para obtener la contraseña del usuario.
     * @return Contraseña del usuario.
     */
    public String getContraseña() {
        return this.contraseña;
    }
    
    /**
     * Método para obtener el permiso de usuario.
     * @return Permiso de usuario.
     */
    public String getPermisoUsuario() {
        return this.permisoUsuario;
    }
    
    /**
     * Método que modifica el Id de usuario.
     * @param nuevoId Nuevo Id de usuario.
     */
    public void modificarId(String nuevoId) {
        this.idUsuario = nuevoId;
    }
    
    /**
     * Método que cambia la contraseña del usuario.
     * @param nuevaContraseña Nueva contraseña del usuario.
     */
    public void cambiarContraseña(String nuevaContraseña) {
        this.contraseña = nuevaContraseña;
    }
    
    /**
     * Método que modifica los permisos de usuario.
     * @param nuevoPermiso Nuevo permiso de usuario.
     */
    public void setPermisoUsuario(String nuevoPermiso) {
        this.permisoUsuario = nuevoPermiso;
    }
    
    /**
     * Método que verifica si una cadena es la contraseña del usuario.
     * @param password Cadena a comprar con la contraseña del usuario.
     * @return true cuando password es igual a la contraseña del usuario, 
     *         false cuando password es diferente de la contraseña del usuario.
     */
    public boolean contraseñaCorrecta (String password) {
        return this.contraseña.equals(password);
    }
    
    /**
     * Método que transforma al usuario en el formato 
     * que tendrá en el archivo login.txt.
     * @return formato del usuario en login.txt
     */
    public String convertirFormatoLogin() {
        String formatoLogin = this.idUsuario + ":" +
                              this.contraseña + ":" +
                              this.permisoUsuario + ":" + 
                              this.nombre + " " + this.apellido;
        return formatoLogin;
    }
    
   /**
    * Método que verifica si dos usuarios son los mismos.
    * @param obj Usuario con el que se va a comparar.
    * @return true cuando todos los atributos de obj son los mismos que los del usuario,
    *         false cuando alguno de los atributos de obj difiere con el correspondiente del usuario
    */
    @Override
    public boolean equals(Object obj) {
        boolean sonIguales = false;
        if (obj instanceof Usuario) {
            Usuario user = (Usuario) obj;
            sonIguales = this.nombre == user.nombre &&
                    this.apellido == user.apellido &&
                    this.idUsuario == user.idUsuario &&
                    this.contraseña == user.contraseña &&
                    this.permisoUsuario == user.permisoUsuario;
        }
        return sonIguales;
    }
    
    
    @Override
    public String toString() {
        return "Nombre: " + this.nombre + " " + this.apellido + ".\n" +
               "Id de usuario: " + this.idUsuario + ".\n" +
               "Privilegio de usuario: " + this.permisoUsuario + ".";
    }

}
