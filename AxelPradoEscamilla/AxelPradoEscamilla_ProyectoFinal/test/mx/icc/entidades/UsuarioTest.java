package mx.icc.entidades;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * Pruebas unitarias de la clase Usuario.
 * @author Axel Prado
 * @version 1.0 28/11/2019
 */
public class UsuarioTest {
    
    /**
     * Prueba la construcción de un Usuario.
     */
    @Test
    public void seCreaUsuario() {
        Usuario axel = new Usuario("Axel","Prado","axel","ocelot","admin");
        assertNotNull(axel);
    }
    
    /**
     * Prueba que el método modificarId modifique el atributo idUsuario.
     */
    @Test
    public void seModificaIdUsuario() {
        Usuario axel = new Usuario("Axel","Prado","axel","ocelot","admin");
        String nuevoId = "AXELPRADO";
        axel.modificarId(nuevoId);
        assertTrue(axel.getIdUsuario().equals(nuevoId));
    }
    
    /**
     * Prueba que el método cambiarContraseña cambie la contraseña de Usuario.
     */
    @Test
    public void seModificaContraseña() {
        Usuario axel = new Usuario("Axel","Prado","axel","ocelot","admin");
        String nuevaContraseña = "snake";
        axel.cambiarContraseña(nuevaContraseña);
        assertTrue(axel.getContraseña().equals(nuevaContraseña));
    }
    
    /**
     * Prueba del método contraseñaCorrecta.
     * Si se le pasa una contraseña incorrecta debe regresar false.
     * Si se le pasa una contraseña correcta, debe regresar true.
     */
    @Test
    public void decideSiContraseñaEsCorrecta() {
        Usuario axel = new Usuario("Axel","Prado","axel","ocelot","admin");
        String contraseñaIncorrecta = "snake";
        String contraseñaCorrecta = "ocelot";
        assertTrue(axel.contraseñaCorrecta(contraseñaCorrecta) &&
                   !axel.contraseñaCorrecta(contraseñaIncorrecta));
    }
    
    /**
     * Prueba el método convertirFormatoLogin.
     * Prueba que regrese el formato adecuado para insertarlo 
     * en el archivo login.txt.
     */
    @Test
    public void formatoLoginCorrecto() {
        Usuario axel = new Usuario("Axel","Prado","axel","ocelot","admin");
        String formatoCorrecto = "axel:ocelot:admin:Axel Prado";
        String formatoObtenido = axel.convertirFormatoLogin();
        assertTrue(formatoObtenido.equals(formatoCorrecto));
    }
    
    /**
     * Prueba del método equals.
     * Verifica igualdad de Usuarios.
     */
    @Test
    public void usuariosIguales() {
        Usuario axel = new Usuario("Axel","Prado","axel","ocelot","admin");
        Usuario user = new Usuario("Axel","Prado","axel","ocelot","admin");
        assertTrue(axel.equals(user));
    }
}
