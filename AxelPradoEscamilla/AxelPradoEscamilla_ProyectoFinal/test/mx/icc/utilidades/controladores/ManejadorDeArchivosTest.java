package mx.icc.utilidades.controladores;

import java.io.*;
import java.util.*;
        
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Test;

/**
 * Pruebas unitarias de la clase ManejadorDeArchivos.
 * @author Axel Prado
 * @version 1.0 28/11/2019
 */
public class ManejadorDeArchivosTest {
    
    /**
     * Prueba del constructor del ManejadorDeArchivos.
     */
    @Test
    public void seCreaManejador() throws IOException {
        File archivo = new File("../data-test/users.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        assertNotNull(manager);
    }
    
    /**
     * Prueba que al construir un manejador con un archivo no vacío,
     * el método obtenerListaLineas construye una lista no vacía.
     * @throws IOException 
     */
    @Test
    public void obtieneArregloNoVacío() throws IOException {
        File archivo = new File("../data-test/users.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        ArrayList<String> lista = manager.obtenerListaLineas();
        assertFalse(lista.isEmpty());
    }
    
    /**
     * Prueba que al construir un manejador con un archivo vacío,
     * el método obtenerListalineas devuelve una lista vacía.
     * @throws IOException 
     */
    @Test
    public void obtieneArregloVacío() throws IOException {
        File archivo = new File("../data-test/vacío.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        ArrayList<String> lista = manager.obtenerListaLineas();
        assertTrue(lista.isEmpty());
    }
    
    /**
     * Prueba que al construir un manejador con un archivo no vacío,
     * el método obtenerListaLineas si devuelva una lista 
     * con las lineas del archivo.
     * @throws IOException 
     */
    @Test
    public void obtieneArregloLineasArchivo() throws IOException {
        File archivo = new File("../data-test/users.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        ArrayList<String> lista = manager.obtenerListaLineas();
        int lineaArchivo = 2;
        assertTrue(lista.get(lineaArchivo - 1).equals("norma:456234:user:Norma Pérez"));
    }
    
    /**
     * Prueba que el método númeroDelineas proporceione el número correcto 
     * de lineas de un archivo.
     * @throws IOException
     */
    @Test
    public void númeroCorrectoDeLineas() throws IOException{
        File archivo = new File("../data-test/users-fijo.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        int numLineas = manager.númeroDeLineas();
        int numLineasEsperado = 5;
        assertTrue(numLineas == numLineasEsperado);
    }
    
    
    /**
     * Prueba que el método insertarLinea inserte una cadena de texto
     * cuando el archivo es vacío.
     * @throws IOException 
     */
    /*@Test
    public void insertaLineaArchivoVacío() throws IOException {
        File archivo = new File("../data-test/datos.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        String nuevaLinea = "axel:ocelot:admin:Axel Prado";
        int posLinea = 4;
        manager.insertarLinea(nuevaLinea,posLinea);
        ArrayList<String> lista = manager.obtenerListaLineas();
        assertTrue(lista.get(0).equals(nuevaLinea));
    }*/
    
    /**
     * Prueba que el método insertarLinea inserte una cadena de texto
     * al final del archivo cuando el número de linea que se le pasa 
     * es mayor que el número de lineas del archivo.
     * @throws IOException 
     */
    @Test
    public void insertaLineaFinalArchivo() throws IOException {
        File archivo = new File("../data-test/users.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        String nuevaLinea = "axel:ocelot:admin:Axel Prado";
        int posLinea = 10;
        manager.insertarLinea(nuevaLinea,posLinea);
        ArrayList<String> lista = manager.obtenerListaLineas();
        int numLineas = manager.númeroDeLineas();
        assertTrue(lista.get(numLineas - 1).equals(nuevaLinea));
    }
    
    /**
     * Prueba que el método insertarLinea inserte una cadena de texto
     * en la posición indicada, siempre que esta sea menor 
     * que el número de lineas.
     * @throws IOException 
     */
    @Test
    public void insertaLineaEnIndiceIndicado() throws IOException {
        File archivo = new File("../data-test/users.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        String nuevaLinea = "snake:ocelot:admin:Juan Dirac";
        int posLinea = 4;
        manager.insertarLinea(nuevaLinea,posLinea);
        ArrayList<String> lista = manager.obtenerListaLineas();
        assertTrue(lista.get(posLinea - 1).equals(nuevaLinea));
    }
    
    /**
     * Prueba que el método eliminarLinea elimine una cadena de texto
     * en la posición indicada, siempre que esta sea menor 
     * que el número de lineas.
     * @throws IOException 
     */
    @Test
    public void eliminaLineaEnIndiceIndicado() throws IOException {
        File archivo = new File("../data-test/users.txt");
        ManejadorDeArchivos manager = new ManejadorDeArchivos(archivo);
        ArrayList<String> lista = manager.obtenerListaLineas();
        int posLinea = 4;
        String lineaCuatro = lista.get(posLinea -1);
        String lineaEliminada = manager.eliminarLinea(posLinea);
        assertTrue(lineaCuatro.equals(lineaEliminada));
    }
    
}
