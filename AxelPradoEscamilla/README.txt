# Proyecto Final: Buscador de cadenas en texto.  

## Nombre: Prado Escamilla Axel.
## No. Cuenta: 310176604.
### Versión java: 11.0.4.
### Versión NetBeans: Apache NetBeans 10.0.

### Paquetes.

  #### entidades.
  Este paquete contiene las clases para representar a un usuario estandar y a un administrador.
    ##### Clases.
   
      ###### Usuario:  
      Esta clase permite simular un usuario de la aplicación con permiso para usarla.
      - Los atributos de esta clase son:
          1.- nombre: representa el nombre del usuario y es de tipo String. El nombre del usuario es una constante.
          2.- apellido: representa el apellido del usuario y es de tipo String. El apellido del usuario es una constante.
          3.- idUsuario: representa el nombre con el cual esta registrado el usuario y es de tipo String.
          4.- contraseña: la contraseña es una concatenación de caracteres, es decir, un String.
          5.- permisoUsuario: hay dos tipos de permisos: "user" y "admin" representados por una cadena (String).
      - Constructores: 
          Solo se usa un constructor de 5 parámetros tipo String que inicializa los 5 atributos de Usuario.
      - Los métodos de está clase son (además de los getters, setters, equals y toString):
          1.- modificarId: es el método set para el IdUsuario. Recibe cadena (Id nuevo) y no tiene tipo de retorno.
          2.- cambiarContraseña: es el método set para la contraseña. Recibe cadena (nueva contraseña) y no tiene tipo de retorno.
          3.- contraseñaCorrecta: este método recibe una cadena (contraseña a comparar) y verifica que sea igual a la contraseña del usuario. 
              Regresa true cuando la cadena que recibe el método es la contraseña del usuario, false en otro caso.
          4.- convertirFormatoLogin: El formato para el archivo login.txt es: "idUsuario:contraseña:permisoUsuario:nombre apellido".
              Este método convierte al usuario en el formato que tendrá en el login.txt.

  #### utilidades.
  Este paquete contiene los subpaquetes que permiten manejar las salidas, las entradas, los usuarios, además, contiene el algoritmo
  para la busqueda de patrones en un texto. Los subpaquetes son:
    
    #### controladores.
    Este paquete permite controlar los datos de los archivos de entrada, controlar los usuarios y el proceso de Login.
      ##### Clases.
      
        ###### ManejadorDeArchivos:
        Esta clase contiene algoritmos para poder manipular las lineas de un archivo de texto.
        - Atributos:
            1.- archivo: representa el archivo que va a ser gestionado, es de tipo File y servirá para crear un FileReader o un FileWriter.
        - Constructores:
            Solo se implementa un constructor de un parámetro tipo File para inicializar el atributo archivo.
            Este constructor verifica que el File parámetro exista para asignarlos al atributo. Si no existe, crea un archivo.
        - Métodos:
            1.- obtenerListaLineas: Regresa un ArrayList<String> de todas las lineas del documento. Cada linea de cadena de texto del archivo
                es un elemento de lña lista. Cuando el File del atributo está vacío, regresa una lista vacía.
                Un ArrayList permite una manipulación más cómoda de los datos del archivo. No recibe ningún parámetro.
            2.- númeroDeLineas: regresa el número de lineas de texto de un archivo.
                Primero se obtiene el ArrayList para luego devolver su tamaño.
            4.- eliminarLinea: Recibe el número de linea a eliminar del archivo. Se obtiene primero el ArrayList de las lineas
                y se usan los métodos de la clase Arraylist. Solo elimina la linea si el número de linea es menor que el tamaño de la lista o
                si la lista no es vacía.
            5.- insertarLinea: Recibe una cadena y un número de linea en donde insertar la cadena en el archivo.  
                Si el archivo es vacío, se inserta una linea. Si el número de linea donde se insertará la cadena es mayor que el
                tamaño de la lista, se inserta una linea al final del archivo.
          
        ##### ListaDeUsuarios:
        Esta clase es un tipo de manejador de archivos especial. Estará diseñanada pra poder manejar el archivo login.txt, es decir, 
        los métodos definidos en esta clase son para manejar lineas de texto de la forma "idUsuario:contraseña:permisoUsuario:nombre apellido".
        Además permite obtener arreglos adecuados para poder autenticar a los usuarios registrados.
        - Atributos:
            1.- archivoUsuarios: Es de tipo File y se supondrá que tiene el formato dado en la especificación del proyecto.
            2.- manejadorArchivo: Como el primer atributo es un archivo, necesitamos un componente quer nos permita manejar el archivo.
        - Métodos:
        Además de los heredados se tienen los siguientes métodos:
            1.- obtenerListaId: este método regresa una lista de todos los id de usuarios registrados en el archivo login.txt.
                Esta lista permite decidir si un usuario ya está registrado.
            2.- obtenerListaUsuarios: A partir de las lineas del login.txt, podemos construir objetos del tipo Usuario y meterlos en una 
                lista de usuarios.
            3.- obtenerHashMapUsuarios: Este método nos regresa un HashMap donde las claves son los id de usuarios y los valores las contraseñas.
                El HasMap facilitará la verficiación de un usuario y la contraseña al momento de iniciar sesión.
                  









